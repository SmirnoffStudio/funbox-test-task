$( document ).ready(function(){
    $('.card-flip').flip();

    $('.cart').on('click', function () {
        var id = $(this).attr("id");

        if($(this).hasClass('active')){
            $(this).removeClass('active');
            $(".conditions[data-id="+id+"] .cart-description").removeClass('active');
            $(".conditions[data-id="+id+"] .cart-buy").removeClass('deactivate');
            $('.cart#'+id).removeClass('hover-active');
            $('.cart#'+id+' .title-selected').css('display', 'block');
            $('.cart#'+id+' .title-selected-hover').css('display', 'none');
            $(".card-flip[data-id="+id+"]").flip(false);
        }else{
            $(this).addClass('active');
            $(".conditions[data-id="+id+"] .cart-description").addClass('active');
            $(".conditions[data-id="+id+"] .cart-buy").addClass('deactivate');
            $(".card-flip[data-id="+id+"]").flip(true);
        }
    });

    $('.cart').mouseover(function () {
        var id = $(this).attr("id");

        if($(this).hasClass('active') && $(this).hasClass('hover-active')){
            $('.cart#'+id+' .title-selected').css('display', 'none');
            $('.cart#'+id+' .title-selected-hover').css('display', 'block');
        }
    });

    $('.cart').mouseleave(function () {
        var id = $(this).attr("id");

        if($(this).hasClass('active')){
            $('.cart#'+id+' .title-selected').css('display', 'block');
            $('.cart#'+id+' .title-selected-hover').css('display', 'none');
            $('.cart#'+id).addClass('hover-active');
        }
    });

    $('.buy').on('click', function () {
        var dataId = $(this).parent().parent().data("id");

        $('.cart#'+dataId).trigger('click');
    });
});