'use strict';
var gulp = require('gulp'),
	watch = require('gulp-watch'),
	preFixer = require('gulp-autoprefixer'),
	uglify = require('gulp-uglify'),
	sass = require('gulp-sass'),
	sourceMaps = require('gulp-sourcemaps'),
	rigger = require('gulp-rigger'),
	cssMin = require('gulp-clean-css'),
	rimRaf = require('rimRaf'),
    imagemin = require('gulp-imagemin'),
	browserSync = require('browser-sync'),
	reload = browserSync.reload;

var path = {
	build: {
		html: 	'build/',
		js: 	'build/js/',
		css: 	'build/style/',
		fonts:  'build/fonts/',
		img:    'build/img/'
	},
	src: {
		html: 	'dist/*.html',
		js: 	'dist/js/main.js',
		style: 	'dist/style/main.scss',
		fonts:  'dist/fonts/*',
        img:    'dist/img/**/*'
	},
	watch: {
		html: 	'dist/**/*.html',
		js: 	'dist/js/**/*.js',
		style: 	'dist/style/**/*.scss',
        img:    'dist/img/**/*'
	},
	clean: './build'
};

gulp.task("webserver", function(){
	browserSync({
		server:{
			baseDir:"./build"
		},
		host: "localhost",
		port: 8080,
		open: true,
    	notify: false
	});
});

gulp.task("html:build", function(){
	return gulp.src(path.src.html)
		.pipe(rigger())
		.pipe(gulp.dest(path.build.html))
		.pipe(reload({stream:true}));
});

gulp.task("js:build", function(){
	return gulp.src(path.src.js)
		.pipe(rigger())
		.pipe(sourceMaps.init())
		.pipe(uglify())
		.pipe(sourceMaps.write())
		.pipe(gulp.dest(path.build.js))
		.pipe(reload({stream:true}));
});

gulp.task("style:build", function(){
	return gulp.src(path.src.style)
		.pipe(sourceMaps.init())
		.pipe(sass())
		.pipe(preFixer())
		.pipe(cssMin())
		.pipe(sourceMaps.write())
		.pipe(gulp.dest(path.build.css))
		.pipe(reload({stream:true}));
});

gulp.task("fonts:build", function () {
	return gulp.src(path.src.fonts)
		.pipe(gulp.dest(path.build.fonts))
        .pipe(reload({stream:true}));
});

gulp.task('imgMin:build', function() {
    return gulp.src(path.src.img)
        .pipe(imagemin({interlaced: true,
            			progressive: true,
						optimizationLevel: 5}))
        .pipe(gulp.dest(path.build.img))
        .pipe(reload({stream:true}));
});

gulp.task("build",[
	"html:build",
	"js:build",
	"style:build",
	"fonts:build",
	"imgMin:build"
]);

gulp.task("watch", function(){
	watch([path.watch.js], function(ev, callback){
		gulp.start('js:build');
	});
	watch([path.watch.html], function(ev, callback){
		gulp.start('html:build');
	});
	watch([path.watch.style], function(ev, callback){
		gulp.start('style:build');
	});
    watch([path.watch.img], function(ev, callback){
        gulp.start('imgMin:build');
    });
});

gulp.task('clean', function(callback){
	rimRaf(path.clean, callback);
});

//gulp.task('default', ['build', 'webserver', 'watch']);

gulp.task('default', ['webserver', 'watch']);
	
